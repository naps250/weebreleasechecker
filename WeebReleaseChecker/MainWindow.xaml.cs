﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ConsoleTest;
using ConsoleTest.XmlSerializationModels;
using WeebReleaseChecker.ViewModels;

namespace WeebReleaseChecker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const double _heightScale = 0.5d;
        private const double _widthScale = 0.5d;


        public MainWindow()
        {
            InitializeComponent();

            this.Height = (SystemParameters.PrimaryScreenHeight * _heightScale);
            this.Width = (SystemParameters.PrimaryScreenWidth * _widthScale);

            Loaded += delegate
            {
                // Sets the width of the tabs panel
                this.ResultsTab.Width = this.LayoutRoot.ColumnDefinitions[0].ActualWidth;
            };
        }
    }
}
