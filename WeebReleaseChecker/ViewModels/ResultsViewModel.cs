﻿using System.Threading.Tasks;
using System.Windows;
using ConsoleTest;
using ConsoleTest.XmlSerializationModels;

namespace WeebReleaseChecker.ViewModels
{
    public class ResultsViewModel : ViewModelBase
    {
        private const string UPDATE = "Update";
        private const string UPDATING = "Updating...";

        private string _updateButtonText = UPDATE;
        private bool _canUserInitiateAnUpdate = true;

        private readonly ReleaseChecker _checker;
        private readonly Results _results;

        private AnimeViewModel _animeViewModel = new AnimeViewModel();
        private MangaViewModel _mangaViewModel = new MangaViewModel();

        public AnimeViewModel AnimeViewModel
        {
            get
            {
                return _animeViewModel;
            }
            set
            {
                if (_animeViewModel != value)
                {
                    _animeViewModel = value;
                    RaisePropertyChanged("AnimeViewModel");
                }
            }
        }

        public MangaViewModel MangaViewModel
        {
            get
            {
                return _mangaViewModel;
            }
            set
            {
                if (_mangaViewModel != value)
                {
                    _mangaViewModel = value;
                    RaisePropertyChanged("MangaViewModel");
                }
            }
        }

        public RelayCommand UpdateResultsCommand { get; set; }

        public string UpdateButtonText
        {
            get
            {
                return _updateButtonText;
            }
            set
            {
                if (_updateButtonText != value)
                {
                    _updateButtonText = value;
                    RaisePropertyChanged("UpdateButtonText");
                }
            }
        }

        public bool CanUserInitiateAnUpdate
        {
            get
            {
                return _canUserInitiateAnUpdate;
            }
            set
            {
                if (_canUserInitiateAnUpdate != value)
                {
                    _canUserInitiateAnUpdate = value;
                    RaisePropertyChanged("CanUserInitiateAnUpdate");
                }
            }
        }

        public ResultsViewModel()
        {
            // TODO: finish layout design
            // TODO: add functionality for the rest of the windows

            _checker = new ReleaseChecker();
            _results = new Results();

            UpdateResultsCommand = new RelayCommand(UpdateResults);

            _results.LoadResults();
            var results = _results.GetResults;
            AnimeViewModel = new AnimeViewModel(results.NewReleases.Anime, results.UpToDate.Anime, results.Issues.AnimeIssues);
            MangaViewModel = new MangaViewModel(results.NewReleases.Manga, results.UpToDate.Manga, results.Issues.MangaIssues);
        }

        void UpdateResults(object parameter)
        {
            CanUserInitiateAnUpdate = false;
            UpdateButtonText = UPDATING;

            var task = Task.Run(() =>
            {
                _checker.Start();
                _results.LoadResults();
            });
            task.ContinueWith(t =>
            {
                var results = _results.GetResults;
                AnimeViewModel = new AnimeViewModel(results.NewReleases.Anime, results.UpToDate.Anime, results.Issues.AnimeIssues);
                MangaViewModel = new MangaViewModel(results.NewReleases.Manga, results.UpToDate.Manga, results.Issues.MangaIssues);

                CanUserInitiateAnUpdate = true;
                UpdateButtonText = UPDATE;
            });
        }
    }
}
