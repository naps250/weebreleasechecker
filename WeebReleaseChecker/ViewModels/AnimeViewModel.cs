﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using ConsoleTest.XmlSerializationModels;

namespace WeebReleaseChecker.ViewModels
{
    public class AnimeViewModel : ViewModelBase
    {
        private ObservableCollection<Anime> _newReleaseAnime = new ObservableCollection<Anime>();

        private ObservableCollection<AnimeBase> _upToDateAnime = new ObservableCollection<AnimeBase>();

        private ObservableCollection<AnimeIssue> _mangaIssues = new ObservableCollection<AnimeIssue>();

        public ObservableCollection<Anime> NewReleaseAnime
        {
            get
            {
                return _newReleaseAnime;
            }
            set
            {
                if (_newReleaseAnime != value)
                {
                    _newReleaseAnime = value;
                    RaisePropertyChanged("NewReleaseAnime");
                }
            }
        }

        public ObservableCollection<AnimeBase> UpToDateAnime
        {
            get
            {
                return _upToDateAnime;
            }
            set
            {
                if (_upToDateAnime != value)
                {
                    _upToDateAnime = value;
                    RaisePropertyChanged("UpToDateAnime");
                }
            }
        }

        public ObservableCollection<AnimeIssue> AnimeIssues
        {
            get
            {
                return _mangaIssues;
            }
            set
            {
                if (_mangaIssues != value)
                {
                    _mangaIssues = value;
                    RaisePropertyChanged("AnimeIssues");
                }
            }
        }

        public AnimeViewModel()
        {
        }

        public AnimeViewModel(IEnumerable<Anime> newReleases, IEnumerable<AnimeBase> upToDate, IEnumerable<AnimeIssue> mangaIssues)
        {
            NewReleaseAnime = new ObservableCollection<Anime>(newReleases);
            UpToDateAnime = new ObservableCollection<AnimeBase>(upToDate);
            AnimeIssues = new ObservableCollection<AnimeIssue>(mangaIssues);
        }
    }
}
