﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using ConsoleTest.XmlSerializationModels;

namespace WeebReleaseChecker.ViewModels
{
    public class MangaViewModel : ViewModelBase
    {
        private ObservableCollection<Manga> _newReleaseManga = new ObservableCollection<Manga>();

        private ObservableCollection<MangaBase> _upToDateManga = new ObservableCollection<MangaBase>();

        private ObservableCollection<MangaIssue> _mangaIssues = new ObservableCollection<MangaIssue>();

        public ObservableCollection<Manga> NewReleaseManga
        {
            get
            {
                return _newReleaseManga;
            }
            set
            {
                if (_newReleaseManga != value)
                {
                    _newReleaseManga = value;
                    RaisePropertyChanged("NewReleaseManga");
                }
            }
        }

        public ObservableCollection<MangaBase> UpToDateManga
        {
            get
            {
                return _upToDateManga;
            }
            set
            {
                if (_upToDateManga != value)
                {
                    _upToDateManga = value;
                    RaisePropertyChanged("UpToDateManga");
                }
            }
        }

        public ObservableCollection<MangaIssue> MangaIssues
        {
            get
            {
                return _mangaIssues;
            }
            set
            {
                if (_mangaIssues != value)
                {
                    _mangaIssues = value;
                    RaisePropertyChanged("MangaIssues");
                }
            }
        }

        public MangaViewModel()
        {
        }

        public MangaViewModel(IEnumerable<Manga> newReleases, IEnumerable<MangaBase> upToDate, IEnumerable<MangaIssue> mangaIssues)
        {
            NewReleaseManga = new ObservableCollection<Manga>(newReleases);
            UpToDateManga = new ObservableCollection<MangaBase>(upToDate);
            MangaIssues = new ObservableCollection<MangaIssue>(mangaIssues);
        }
    }
}
