﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using ConsoleTest.XmlSerializationModels;
using WeebReleaseChecker.ViewModels;

namespace WeebReleaseChecker.Controls
{
    /// <summary>
    /// Interaction logic for AnimeResultsView.xaml
    /// </summary>
    public partial class AnimeResultsView : UserControl
    {
        public AnimeResultsView()
        {
            InitializeComponent();
        }
    }
}
