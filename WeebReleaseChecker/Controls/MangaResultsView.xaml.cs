﻿using System;
using System.Windows.Controls;
using WeebReleaseChecker.ViewModels;

namespace WeebReleaseChecker.Controls
{
    /// <summary>
    /// Interaction logic for MangaResultsView.xaml
    /// </summary>
    public partial class MangaResultsView : UserControl
    {
        public MangaResultsView()
        {
            InitializeComponent();
        }
    }
}
