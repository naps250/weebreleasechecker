﻿using System.ComponentModel;
using System.Windows.Controls;

namespace WeebReleaseChecker.Controls
{
    /// <summary>
    /// Interaction logic for ResultsDataGridView.xaml
    /// </summary>
    public partial class ResultsDataGridView : UserControl
    {
        public ResultsDataGridView()
        {
            InitializeComponent();
        }

        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)e.PropertyDescriptor;
            e.Column.Header = propertyDescriptor.DisplayName;
            if (propertyDescriptor.DisplayName == "MalId")
            {
                e.Column.Visibility = System.Windows.Visibility.Hidden;
            }
        }
    }
}
