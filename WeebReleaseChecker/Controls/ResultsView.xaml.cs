﻿using System;
using System.Windows;
using System.Windows.Controls;
using ConsoleTest;
using WeebReleaseChecker.ViewModels;

namespace WeebReleaseChecker.Controls
{
    /// <summary>
    /// Interaction logic for ResultsView.xaml
    /// </summary>
    public partial class ResultsView : UserControl
    {
        public static readonly DependencyProperty ChildResultsSetDeeDependencyProperty =
            DependencyProperty.Register("ChildResultsProperty", typeof(Results), typeof(UserControl), new FrameworkPropertyMetadata(null));

        public Results ChildResultsProperty
        {
            get { return (Results)GetValue(ChildResultsSetDeeDependencyProperty); }
            set { SetValue(ChildResultsSetDeeDependencyProperty, value); }
        }


        public ResultsView()
        {
            InitializeComponent();
        }
    }
}
