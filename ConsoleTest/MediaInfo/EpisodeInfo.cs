﻿using System;

namespace ConsoleTest.MediaInfo
{
    public class EpisodeInfo : MediaInfoBase
    {
        public string EpisodeNum { get; set; }

        public EpisodeInfo()
        {
        }

        public EpisodeInfo(string name, string episodeNum, DateTime uploadDate) : base(name, uploadDate)
        {
            EpisodeNum = episodeNum;
        }
    }
}
