﻿using System;

namespace ConsoleTest.MediaInfo
{
    public abstract class MediaInfoBase
    {
        public string Name { get; set; }

        public DateTime UploadDate { get; set; }

        protected MediaInfoBase()
        {
        }

        protected MediaInfoBase(string name, DateTime uploadDate)
        {
            Name = name;
            UploadDate = uploadDate;
        }
    }
}
