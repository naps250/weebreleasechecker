﻿using System;
using ConsoleTest.XmlDeserializationModels;

namespace ConsoleTest.MediaInfo
{
    public class ChapterInfo : MediaInfoBase
    {
        public string ChapterNum { get; set; }

        public ChapterInfo()
        {
        }

        public ChapterInfo(string name, string chapterNum, DateTime uploadDate) : base(name, uploadDate)
        {
            ChapterNum = chapterNum;
        }
    }
}
