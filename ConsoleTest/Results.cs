﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using ConsoleTest.XmlSerializationModels;

namespace ConsoleTest
{
    public class Results
    {
        private UpdateResults _results = new UpdateResults();

        public UpdateResults GetResults
        {
            get
            {
                return _results;
            }
        }

        public void LoadResults()
        {
            var path = Properties.Settings.Default.ResultPath;

            bool exists = File.Exists(path);
            if (exists)
            {
                var serializer = new XmlSerializer(typeof(UpdateResults));

                using (var reader = new StreamReader(path))
                {
                    _results = (UpdateResults)serializer.Deserialize(reader);
                }
            }
        }

    }
}
