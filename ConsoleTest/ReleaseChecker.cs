﻿using System;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using ConsoleTest.XmlDeserializationModels;
using ConsoleTest.XmlSerializationModels;
using Manga = ConsoleTest.XmlSerializationModels.Manga;
using Anime = ConsoleTest.XmlSerializationModels.Anime;

namespace ConsoleTest
{
    public class ReleaseChecker
    {
        private readonly UpdateResults _results = new UpdateResults();

        public void Start()
        {
            CheckForUpdates();

            SaveResults(Properties.Settings.Default.ResultPath);
        }

        private void CheckForUpdates()
        {
            // Get currently reading/watching lists from MAL
            var malAnimeInfo = MalUtils.GetMalAnimeWatchingListInfo();
            var malMangaInfo = MalUtils.GetMalMangaReadingListInfo();

            // Get fresh lists from the configuration file
            var configuration = Configuration.ReadConfig();

            #region Process the manga lists

            // At this point it's only KissManga and KissAnime
            // Processing
            foreach (var website in configuration.WebsitesColl.Websites)
            {
                website.GetReleases();

                foreach (var manga in website.Manga)
                {
                    foreach (var malManga in malMangaInfo)
                    {
                        if (CompareManga(manga, true, malManga: malManga))
                        {
                            break;
                        }
                    }
                    foreach (var notOnMalRead in configuration.NotOnMalColl.Read)
                    {
                        if (CompareManga(manga, false, notOnMalRead: notOnMalRead))
                        {
                            break;
                        }
                    }
                }
            }
            // Reporting
            foreach (var website in configuration.WebsitesColl.Websites)
            {
                var message = string.Empty;
                foreach (var manga in website.Manga)
                {
                    if (!manga.Matched)
                    {
                        message = $"No match found for XML {website.Name} manga record.";
                        _results.Issues.MangaIssues.Add(new MangaIssue
                        {
                            MalId = -1,
                            Title = manga.Title,
                            Message = message
                        });
                        // Console.WriteLine($"No match found for {website.Name} manga record {manga.Title}.");
                    }
                }
            }
            foreach (var malManga in malMangaInfo)
            {
                var message = string.Empty;
                if (!malManga.Matched)
                {
                    message = "No match found for MAL manga record.";
                    _results.Issues.MangaIssues.Add(new MangaIssue
                    {
                        MalId = -1,
                        Title = malManga.MangaInfo.Title,
                        Message = message
                    });
                    // Console.WriteLine($"No match found for MAL manga record {malManga.MangaInfo.Title}.");
                }
            }
            foreach (var notOnMalRead in configuration.NotOnMalColl.Read)
            {
                var message = string.Empty;
                if (!notOnMalRead.Matched)
                {
                    message = "No match found for XML NotOnMal read record.";
                    _results.Issues.MangaIssues.Add(new MangaIssue
                    {
                        MalId = -1,
                        Title = notOnMalRead.Title,
                        Message = message
                    });
                    // Console.WriteLine($"No match found for XML NotOnMal read record {notOnMalRead.Title}.");
                }
            }

            #endregion

            #region Process the anime lists

            // Processing
            foreach (var website in configuration.WebsitesColl.Websites)
            {
                foreach (var anime in website.Anime)
                {
                    foreach (var malAnime in malAnimeInfo)
                    {
                        if (CompareAnime(anime, true, malAnime: malAnime))
                        {
                            break;
                        }
                    }
                    foreach (var notOnMalWatch in configuration.NotOnMalColl.Watch)
                    {
                        if (CompareAnime(anime, false, notOnMalWatch: notOnMalWatch))
                        {
                            break;
                        }
                    }
                }
            }
            // Reporting
            foreach (var website in configuration.WebsitesColl.Websites)
            {
                var message = string.Empty;
                foreach (var anime in website.Anime)
                {
                    if (!anime.Matched)
                    {
                        message = $"No match found for XML {website.Name} anime record.";
                        _results.Issues.AnimeIssues.Add(new AnimeIssue
                        {
                            MalId = -1,
                            Title = anime.Title,
                            Message = message
                        });
                        // Console.WriteLine($"No match found for {website.Name} anime record {anime.Title}.");
                    }
                }
            }
            foreach (var malAnime in malAnimeInfo)
            {
                var message = string.Empty;
                if (!malAnime.Matched)
                {
                    message = "No match found for MAL anime record.";
                    _results.Issues.AnimeIssues.Add(new AnimeIssue
                    {
                        MalId = -1,
                        Title = malAnime.AnimeInfo.Title,
                        Message = message
                    });
                    // Console.WriteLine($"No match found for MAL anime record {malAnime.AnimeInfo.Title}.");
                }
            }
            foreach (var notOnMalWatch in configuration.NotOnMalColl.Watch)
            {
                var message = string.Empty;
                if (!notOnMalWatch.Matched)
                {
                    message = "No match found for XML NotOnMal watch record.";
                    _results.Issues.AnimeIssues.Add(new AnimeIssue
                    {
                        MalId = -1,
                        Title = notOnMalWatch.Title,
                        Message = message
                    });
                    // Console.WriteLine($"No match found for XML NotOnMal watch record {notOnMalWatch.Title}.");
                }
            }

            #endregion
        }

        public bool CompareManga(XmlDeserializationModels.Manga manga, bool isOnMal, MyCustomMangaListEntry malManga = null, NotOnMalRead notOnMalRead = null)
        {
            var title = manga.Title;
            if ((isOnMal && (malManga.MangaInfo.Title.Contains(title) || malManga.MangaInfo.Synonyms.Contains(title)))
             || (!isOnMal && notOnMalRead.Title.Contains(title)))
            {
                manga.Matched = true;
                if (isOnMal)
                {
                    malManga.Matched = true;
                }
                else
                {
                    notOnMalRead.Matched = true;
                }

                // Compare
                var readChapters = isOnMal ? malManga.NumChaptersRead : notOnMalRead.ReadChapters;
                var availableChapters = int.Parse(manga.Chapters[0].ChapterNum);
                if (readChapters < availableChapters)
                {
                    //0 vs 1-based count shenanigans (/sigh)
                    var nextChapterIndex = readChapters + 1;

                    var startFrom = manga.Chapters.First(x => x.ChapterNum.Substring(0, 3).Contains(nextChapterIndex.ToString())).Name;

                    _results.NewReleases.Manga.Add(new Manga
                    {
                        MalId = isOnMal ? malManga.MangaInfo.MangaId : -1,
                        Title = title,
                        AvailableChapters = availableChapters,
                        ReadChapters = readChapters,
                        NextChapter = startFrom
                    });
                }
                else
                {
                    _results.UpToDate.Manga.Add(new Manga
                    {
                        MalId = isOnMal ? malManga.MangaInfo.MangaId : -1,
                        Title = title
                    });
                }

                return true;
            }
            return false;
        }

        public bool CompareAnime(XmlDeserializationModels.Anime anime, bool isOnMal, MyCustomAnimeListEntry malAnime = null, NotOnMalWatch notOnMalWatch = null)
        {
            var title = anime.Title;
            if ((isOnMal && (malAnime.AnimeInfo.Title.Contains(title) || malAnime.AnimeInfo.Synonyms.Contains(title)))
                || (!isOnMal && notOnMalWatch.Title.Contains(title)))
            {
                anime.Matched = true;
                if (isOnMal)
                {
                    malAnime.Matched = true;
                }
                else
                {
                    notOnMalWatch.Matched = true;
                }

                // Compare
                var watchedEpisodes = isOnMal ? malAnime.NumEpisodesWatched : notOnMalWatch.WatchedEpisodes;
                var availableEpisodes = int.Parse(anime.Episodes[0].EpisodeNum);
                if (watchedEpisodes < availableEpisodes)
                {
                    //0 vs 1-based count shenanigans (/sigh)
                    var nextChapterIndex = watchedEpisodes + 1;

                    var startFrom = anime.Episodes.First(x => x.EpisodeNum.Substring(0, 3).Contains(nextChapterIndex.ToString())).Name;

                    _results.NewReleases.Anime.Add(new Anime
                    {
                        MalId = isOnMal ? malAnime.AnimeInfo.AnimeId : -1,
                        Title = title,
                        AvailableEpisodes = availableEpisodes,
                        WatchedEpisodes = watchedEpisodes,
                        NextEpisode = startFrom
                    });
                }
                else
                {
                    _results.UpToDate.Anime.Add(new Anime
                    {
                        MalId = isOnMal ? malAnime.AnimeInfo.AnimeId : -1,
                        Title = title
                    });
                }

                return true;
            }
            return false;
        }

        /// <summary>
        /// Saves the results to an XML file
        /// </summary>
        /// <param name="path">The relative path to the output XML</param>
        private void SaveResults(string path)
        {
            // Create the path, if it does not exist yet
            var index = path.LastIndexOf(@"\", StringComparison.Ordinal);
            var dir = path.Substring(0, index);
            bool exists = Directory.Exists(dir);
            if (!exists)
            {
                Directory.CreateDirectory(dir);
            }

            // Save the update results
            var serializer = new XmlSerializer(typeof(UpdateResults));
            using (var writer = new StreamWriter(path))
            {
                serializer.Serialize(writer, _results);
            }
        }
    }
}
