﻿namespace ConsoleTest
{
    public class Program
    {
        private static void Main()
        {
            var checker = new ReleaseChecker();

            checker.Start();

            var results = new Results();

            results.LoadResults();
        }
        
    }
}