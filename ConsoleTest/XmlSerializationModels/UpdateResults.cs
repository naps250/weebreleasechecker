﻿namespace ConsoleTest.XmlSerializationModels
{
    public class UpdateResults
    {
        public NewReleases NewReleases { get; set; } = new NewReleases();

        public UpToDate UpToDate { get; set; } = new UpToDate();

        public Issues Issues { get; set; } = new Issues();
    }
}
