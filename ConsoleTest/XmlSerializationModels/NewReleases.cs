﻿using System.Collections.Generic;

namespace ConsoleTest.XmlSerializationModels
{
    public class NewReleases : ResultColl
    {
        public List<Anime> Anime { get; set; } = new List<Anime>();

        public List<Manga> Manga { get; set; } = new List<Manga>();
    }
}
