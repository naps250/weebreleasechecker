﻿namespace ConsoleTest.XmlSerializationModels
{
    public abstract class AnimeBase
    {
        public int MalId { get; set; }

        public string Title { get; set; }
    }
}
