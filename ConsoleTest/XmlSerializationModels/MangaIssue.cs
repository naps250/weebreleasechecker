﻿namespace ConsoleTest.XmlSerializationModels
{
    public class MangaIssue : MangaBase
    {
        public string Message { get; set; }

        public override bool Equals(object obj)
        {
            var other = obj as MangaIssue;
            return other != null && other.Title == this.Title && other.Message == this.Message;
        }

        public override int GetHashCode()
        {
            return this.Title.GetHashCode() ^ this.Message.GetHashCode();
        }
    }
}
