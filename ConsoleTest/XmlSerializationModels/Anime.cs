﻿namespace ConsoleTest.XmlSerializationModels
{
    public class Anime : AnimeBase
    {
        public int WatchedEpisodes { get; set; }

        public int AvailableEpisodes { get; set; }

        public string NextEpisode { get; set; }

        public bool ShouldSerializeWatchedEpisodes()
        {
            return WatchedEpisodes != 0;
        }

        public bool ShouldSerializeAvailableEpisodes()
        {
            return AvailableEpisodes != 0;
        }

        public bool ShouldSerializeNextEpisode()
        {
            return NextEpisode != null;
        }
    }
}
