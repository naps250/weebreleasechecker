﻿using System.Collections.Generic;

namespace ConsoleTest.XmlSerializationModels
{
    public class Issues
    {
        public HashSet<AnimeIssue> AnimeIssues { get; set; } = new HashSet<AnimeIssue>();

        public HashSet<MangaIssue> MangaIssues { get; set; } = new HashSet<MangaIssue>();
    }
}
