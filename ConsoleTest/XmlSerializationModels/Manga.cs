﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace ConsoleTest.XmlSerializationModels
{
    [DataContract]
    public class Manga : MangaBase
    {
        [DataMember]
        public int ReadChapters { get; set; }

        [DataMember]
        public int AvailableChapters { get; set; }

        [DataMember]
        public string NextChapter { get; set; }

        public bool ShouldSerializeReadChapters()
        {
            return ReadChapters != 0;
        }

        public bool ShouldSerializeAvailableChapters()
        {
            return AvailableChapters != 0;
        }

        public bool ShouldSerializeNextChapter()
        {
            return NextChapter != null;
        }
    }
}
