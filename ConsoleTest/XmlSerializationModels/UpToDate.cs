﻿using System.Collections.Generic;

namespace ConsoleTest.XmlSerializationModels
{
    public class UpToDate : ResultColl
    {
        public List<AnimeBase> Anime { get; set; } = new List<AnimeBase>();

        public List<MangaBase> Manga { get; set; } = new List<MangaBase>();
    }
}
