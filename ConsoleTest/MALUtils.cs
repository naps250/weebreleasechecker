﻿using MalApi;
using MalApi.Anime;
using MalApi.Manga;
using System.Collections.Generic;

namespace ConsoleTest
{
    public class MyCustomAnimeListEntry : MyAnimeListEntry
    {
        public MyCustomAnimeListEntry(MyAnimeListEntry malAnimeEntry)
        : base(malAnimeEntry.Score,
               malAnimeEntry.Status,
               malAnimeEntry.NumEpisodesWatched,
               malAnimeEntry.MyStartDate,
               malAnimeEntry.MyFinishDate,
               malAnimeEntry.MyLastUpdate,
               malAnimeEntry.AnimeInfo,
               malAnimeEntry.Tags)
        {
        }

        // This is the reason for the object extention...
        public bool Matched { get; set; } = false;
    }

    public class MyCustomMangaListEntry : MyMangaListEntry
    {
        public MyCustomMangaListEntry(MyMangaListEntry malMangaEntry)
        : base(malMangaEntry.Score,
               malMangaEntry.Status,
               malMangaEntry.NumChaptersRead,
               malMangaEntry.NumVolumesRead,
               malMangaEntry.MyStartDate,
               malMangaEntry.MyFinishDate,
               malMangaEntry.MyLastUpdate,
               malMangaEntry.MangaInfo,
               malMangaEntry.Tags)
        {
        }

        // This is the reason for the object extention...
        public bool Matched { get; set; } = false;
    }

    public class MalUtils
    {
        public static List<MyCustomAnimeListEntry> GetMalAnimeWatchingListInfo()
        {
            var currWatching = new List<MyCustomAnimeListEntry>();
            using (var api = new MyAnimeListApi())
            {
                api.UserAgent = "my_app"; // MAL now requires applications to be whitelisted. Whitelisted applications identify themselves by their user agent.
                var userLookup = api.GetAnimeListForUser("naps250");

                // Sample print
                foreach (var listEntry in userLookup.AnimeList)
                {
                    if (listEntry.Status == AnimeCompletionStatus.Watching)
                    {
                        currWatching.Add(new MyCustomAnimeListEntry(listEntry));
                    }
                }
            }

            return currWatching;
        }

        public static List<MyCustomMangaListEntry> GetMalMangaReadingListInfo()
        {
            var currReading = new List<MyCustomMangaListEntry>();
            using (var api = new MyAnimeListApi())
            {
                api.UserAgent = "my_app"; // MAL now requires applications to be whitelisted. Whitelisted applications identify themselves by their user agent.
                var userLookup = api.GetMangaListForUser("naps250");

                // Sample print
                foreach (var listEntry in userLookup.MangaList)
                {
                    if (listEntry.Status == MangaCompletionStatus.Reading)
                    {
                        currReading.Add(new MyCustomMangaListEntry(listEntry));
                    }
                }
            }

            return currReading;
        }
    }
}
