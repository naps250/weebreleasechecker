﻿using HtmlAgilityPack;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using ConsoleTest.MediaInfo;
using ConsoleTest.XmlDeserializationModels;

namespace ConsoleTest.Websites
{
    public class KissMangaClient : WebsiteClient
    {
        public override List<ChapterInfo> GetMangaChapters(HtmlDocument doc)
        {
            //get chapters
            var chapters = doc.DocumentNode
                            .SelectNodes("//table/tr/td/a");
            // get chapter dates
            var dates = doc.DocumentNode
                            .SelectNodes("//table/tr/td")
                            .Where(x => x.ChildNodes.Count == 1).ToList();

            if(chapters.Count != dates.Count)
            {
                throw new Exception("Error extracting information from the page HTML!");
            }

            var chapterInfo = new List<ChapterInfo>();

            var count = chapters.Count;
            for (var i = 0; i < count; i++)
            {
                /*
                 * Regex does the following:
                 * 1) Matches a space or "Ch." or "ch." exactly once            -> (?:(?: |(?:C|c)h.)){1}
                 * 2) Followed by exactly 3 digits                              -> [0-9]{3}
                 * 3) Not followed by a comma (',')                             -> (?!,)
                 * 4) 2) followed by a dot ('.') or a lower 'v'                 -> (?:(?:\.|v)
                 * 5) Followed by zero or one occurance of 1 to 3 digits        -> [0-9]{1,3}
                 * 6) NOT followed by any letter                                -> (?![a-zA-z])
                 * Steps 4 and 5 are matches zero or once                       -> (?:(?:\.|v)[0-9]{1,3}){0,1}
                 * 
                 * Steps 2 through 6 are the needed capture group,
                 * hence the additional brackets around them with               -> ([0-9]{3}(?!,)(?:(?:\.|v)[0-9]{1,3}){0,1}(?![a-zA-z]))
                 */
                var regex = new Regex(@"(?:(?: |(?:C|c)h.)){1}([0-9]{3}(?!,)(?:(?:\.|v)[0-9]{1,3}){0,1}(?![a-zA-z]))", RegexOptions.None);

                // Get chapter text as is since there are too many variations and I can't be bothered to constantly update for all of them
                var text = chapters[i].InnerText;
                text = text.Trim('\n');
                var chapterName = text;
                 // Match
                var match = regex.Match(text);
                // Get chapter number
                var chapter = match.Groups[1].Value;
                // Get the upload date
                text = dates[i].InnerText;
                text = text.Trim('\n');
                var dateArray = text.Split('/');
                var date = new DateTime(int.Parse(dateArray[2]), int.Parse(dateArray[0]), int.Parse(dateArray[1]));

                chapterInfo.Add(new ChapterInfo(chapterName, chapter, date));
            }

            return chapterInfo;
        }
    }
}
