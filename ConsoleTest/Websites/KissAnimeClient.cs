﻿using HtmlAgilityPack;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using ConsoleTest.MediaInfo;
using ConsoleTest.XmlDeserializationModels;

namespace ConsoleTest.Websites
{
    public class KissAnimeClient : WebsiteClient
    {
        public override List<EpisodeInfo> GetAnimeEpisodes(HtmlDocument doc)
        {
            //get chapters
            var episodes = doc.DocumentNode
                .SelectNodes("//table/tr/td/a");
            // get chapter dates
            var dates = doc.DocumentNode
                .SelectNodes("//table/tr/td")
                .Where(x => x.ChildNodes.Count == 1).ToList();

            if (episodes.Count != dates.Count)
            {
                throw new Exception("Error extracting information from the page HTML!");
            }

            var episodeInfo = new List<EpisodeInfo>();

            var count = episodes.Count;
            for (var i = 0; i < count; i++)
            {
                /*
                 * Regex does the following:
                 * 1) Matches the "Episode " string                                     -> Episode 
                 * 2) Followd by exactly 3 digits                                       -> [0-9]{3}
                 * 3) Followed by a dot ('.') and 1 to 3 digits matched zero or once    -> (?:\.[0-9]{1,3}){0,1}
                 * 
                 * Steps 2 and 3 are the needed capture group,
                 * hence the additional brackets around them with               -> ([0-9]{3}(?:\.[0-9]{1,3}){0,1})
                 */
                var regex = new Regex(@"Episode ([0-9]{3}(?:\.[0-9]{1,3}){0,1})", RegexOptions.None);

                // Get episode text as is since there are too many variations and I can't be bothered to constantly update for all of them
                var text = episodes[i].InnerText;
                text = text.Trim('\r', '\n');
                text = text.Trim();
                var episodeName = text;
                // Match
                var match = regex.Match(text);
                // Get episode number
                string episodeNum;
                if (match.Success)
                {
                    episodeNum = match.Groups[1].Value;
                }
                else
                {
                    episodeNum = "N/A";
                }
                // Get the upload date
                text = dates[i].InnerText;
                text = text.Trim('\r', '\n');
                text = text.Trim();
                var dateArray = text.Split('/');
                var date = new DateTime(int.Parse(dateArray[2]), int.Parse(dateArray[0]), int.Parse(dateArray[1]));

                episodeInfo.Add(new EpisodeInfo(episodeName, episodeNum, date));
            }

            return episodeInfo;
        }
    }
}
