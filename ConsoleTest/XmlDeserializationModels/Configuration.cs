﻿using MalApi.Anime;
using MalApi.Manga;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ConsoleTest.XmlDeserializationModels
{
    [Serializable]
    [XmlRoot("Configuration")]
    public class Configuration
    {
        [XmlElement("MALuser")]
        public string MalUser { get; set; }

        [XmlElement("Websites")]
        public WebsitesColl WebsitesColl { get; set; }

        [XmlElement("NotOnMal")]
        public NotOnMalColl NotOnMalColl { get; set; }

        public static Configuration ReadConfig()
        {
            Configuration configuration;

            var configPath = Properties.Settings.Default.ConfigPath;
            var serializer = new XmlSerializer(typeof(Configuration));
            using (var reader = new StreamReader(configPath))
            {
                configuration = (Configuration)serializer.Deserialize(reader);
            }

            // Finish setting up the objects
            configuration.NotOnMalColl.Read = configuration.NotOnMalColl.Read.Where(x => x.Status != MangaCompletionStatus.Completed).ToList();
            configuration.NotOnMalColl.Watch = configuration.NotOnMalColl.Watch.Where(x => x.Status != AnimeCompletionStatus.Completed).ToList();

            /* 
             * Since most sites have CloudFlare DDOS protection that required a set amount of delay using parallel processing here
             * reduces the time needed for multiple websites to that of a single one.
             */
            Parallel.ForEach(configuration.WebsitesColl.Websites, (website) =>
            {
                website.SetUpWebClient();
            });

            return configuration;
        }
    }
}
