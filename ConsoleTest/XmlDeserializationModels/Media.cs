﻿using System.Xml.Serialization;

namespace ConsoleTest.XmlDeserializationModels
{
    public abstract class Media
    {
        [XmlElement("Title")]
        public string Title { get; set; }

        [XmlElement("Url")]
        public string Url { get; set; }

        public bool Matched { get; set; } = false;
    }
}
