﻿using MalApi.Manga;
using System.Xml.Serialization;

namespace ConsoleTest.XmlDeserializationModels
{
    public class NotOnMalRead : NotOnMalMedia
    {
        [XmlElement("ReadChapters")]
        public int ReadChapters { get; set; }

        [XmlElement("Status")]
        public MangaCompletionStatus Status { get; set; }
    }
}
