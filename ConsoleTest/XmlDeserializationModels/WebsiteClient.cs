﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Net;
using System.Xml.Serialization;
using ConsoleTest.MediaInfo;

namespace ConsoleTest.XmlDeserializationModels
{
    public abstract class WebsiteClient
    {
        [XmlElement("Name")]
        public string Name { get; set; }

        [XmlElement("Url")]
        public string Url { get; set; }

        protected List<Anime> _anime = new List<Anime>();
        [XmlElement("Anime", typeof(Anime))]
        public List<Anime> Anime
        {
            get
            {
                return _anime;
            }
            set
            {
                _anime = value;
            }
        }

        protected List<Manga> _manga = new List<Manga>();
        [XmlElement("Manga", typeof(Manga))]
        public List<Manga> Manga
        {
            get
            {
                return _manga;
            }
            set
            {
                _manga = value;
            }
        }

        protected WebClient _client;

        public void SetUpWebClient()
        {
            _client = GetWebClient(Url);
        }

        /// <summary>
        /// Returns the list of media (anime episodes or manga chapters), along with their upload dates
        /// </summary>
        public virtual void GetReleases()
        {
            // Process anime list
            foreach (var anime in Anime)
            {
                var html = _client.DownloadString(anime.Url);

                var doc = new HtmlDocument();
                doc.LoadHtml(html);

                try
                {
                    anime.Episodes = GetAnimeEpisodes(doc);
                }
                catch (Exception e)
                {
                    Console.WriteLine(anime.Title + ": " + e.Message);
                }
            }

            // Process manga list
            foreach (var manga in Manga)
            {
                var html = _client.DownloadString(manga.Url);

                var doc = new HtmlDocument();
                doc.LoadHtml(html);

                try
                {
                    manga.Chapters = GetMangaChapters(doc);
                }
                catch (Exception e)
                {
                    Console.WriteLine(manga.Title + ": " + e.Message);
                }
            }
        }

        public virtual List<EpisodeInfo> GetAnimeEpisodes(HtmlDocument doc)
        {
            throw new NotImplementedException();
        }

        public virtual List<ChapterInfo> GetMangaChapters(HtmlDocument doc)
        {
            throw new NotImplementedException();
        }

        public WebClient GetWebClient(string url)
        {
            WebClient client = null;
            while (client == null)
            {
                Console.WriteLine("Trying..");
                client = CloudflareEvader.CreateBypassedWebClient(url);
            }
            Console.WriteLine("Solved! We're clear to go");

            return client;
        }
    }
}
