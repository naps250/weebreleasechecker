﻿using System.Collections.Generic;
using System.Xml.Serialization;
using ConsoleTest.Websites;

namespace ConsoleTest.XmlDeserializationModels
{
    public class WebsitesColl
    {
        [XmlElement("KissAnime", typeof(KissAnimeClient))]
        [XmlElement("KissManga", typeof(KissMangaClient))]
        public List<WebsiteClient> Websites { get; set; }
    }
}
