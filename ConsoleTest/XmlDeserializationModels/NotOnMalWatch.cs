﻿using MalApi.Anime;
using System.Xml.Serialization;

namespace ConsoleTest.XmlDeserializationModels
{
    public class NotOnMalWatch : NotOnMalMedia
    {
        [XmlElement("WatchedEpisodes")]
        public int WatchedEpisodes { get; set; }

        [XmlElement("Status")]
        public AnimeCompletionStatus Status { get; set; }
    }
}