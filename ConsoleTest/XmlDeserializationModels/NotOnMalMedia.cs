﻿using System.Xml.Serialization;

namespace ConsoleTest.XmlDeserializationModels
{
    public abstract class NotOnMalMedia
    {
        [XmlElement("Title")]
        public string Title { get; set; }

        public bool Matched { get; set; } = false;
    }
}
