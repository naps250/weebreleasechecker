﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ConsoleTest.XmlDeserializationModels
{
    public class NotOnMalColl
    {
        protected List<NotOnMalWatch> _watch = new List<NotOnMalWatch>();
        [XmlElement("Watch", typeof(NotOnMalWatch))]
        public List<NotOnMalWatch> Watch
        {
            get { return _watch; }
            set { _watch = value; }
        }

        protected List<NotOnMalRead> _read = new List<NotOnMalRead>();
        [XmlElement("Read", typeof(NotOnMalRead))]
        public List<NotOnMalRead> Read
        {
            get { return _read; }
            set { _read = value; }
        }
    }
}
