﻿using System.Collections.Generic;
using ConsoleTest.MediaInfo;

namespace ConsoleTest.XmlDeserializationModels
{
    public class Anime : Media
    {
        public List<EpisodeInfo> Episodes { get; set; } = new List<EpisodeInfo>();
    }
}
