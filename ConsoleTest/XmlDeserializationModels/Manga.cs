﻿using System.Collections.Generic;
using ConsoleTest.MediaInfo;

namespace ConsoleTest.XmlDeserializationModels
{
    public class Manga : Media
    {
        public List<ChapterInfo> Chapters { get; set; } = new List<ChapterInfo>();
    }
}
